��             +         �     �     �     �  	   �     �     	          (     6     ;     K     T     `     f     m     z     �     �     �     �     �  Q   �          +     8  
   E     P  
   ^     i  .   o  I   �  �  �  	   k     u     �     �     �     �  
   �     �          	  	   "     ,     >     G     O     a     j     p     �     �  *   �  f   �     =     J     Z     l     }     �     �  8   �  P   �                                   
                                    	                                                                         About Authorities Background procedures Configure Data checkers Data problem messages Explorer Hi, %(user)s! Home Library volumes Mentions My settings My {} Office Quick links: Reports Site Site Parameters System Text Field Templates This is a Lino demo site. Try also the other <a href="http://lino-framework.org/demos.html">demo sites</a>. Upload area Upload areas Upload files User roles User sessions User types Users We are running with simulated date set to {0}. Your feedback is welcome to %s or directly to the person who invited you. Project-Id-Version: lino 1.6.14
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2024-05-14 16:29-0300
Last-Translator: Luc Saffre <luc.saffre@gmx.net>
Language-Team: es <LL@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: Babel 2.12.1
X-Generator: Poedit 3.0.1
 Acerca de Autoridades Procedimientos en segundo plano Configuración Controladores de datos Mensajes de problemas de datos Explorador Hola, %(user)s!  Inicio Volúmenes de biblioteca Menciones Mi configuración Mi(s) {} Oficina Enlaces rápidos: Reportes Sitio Parámetros del Sitio Sistema Plantillas de campo de texto Este es un sitio de demostración de Lino. Prueba también los otros <a href="http://lino-framework.org/demos.html"> sitios de demostración.</a> Cargar área Carga de áreas Carga de archivos Roles de usuario Sesiones de usuario Tipos de usuario Usuarios Estamos corriendo con fecha simulada fijada para el {0}. Tus comentarios son bienvenidos a %s o directamente a la persona que te invitó. 