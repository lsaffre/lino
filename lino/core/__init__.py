"""
Contains Lino's core functionalities.

For some modules the documentation has already been migrated to prosa:

- `plugin` is documented in :doc:`/api/ad/plugin`
- `site` is documented in :doc:`/api/ad/site`

.. autosummary::
   :toctree:

    actions
    actors
    boundaction
    callbacks
    choicelists
    constants
    dashboard
    dbtables
    dbutils
    ddh
    diff
    elems
    exceptions
    fields
    frames
    help
    inject
    kernel
    keyboard
    layouts
    menus
    merge
    model
    permissions
    renderer
    requests
    roles
    signals
    store
    tables
    tablerequest
    urls
    userprefs
    user_types
    utils
    views
    workflows
    auth

"""
