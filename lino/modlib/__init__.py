# Copyright 2008-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""The standard model library included with Lino.

A collection of ready-to-use plugins (Django calls them "apps") for
Lino applications.

This page is being migrated to :ref:`dg.plugins`.

System plugins
==============

.. autosummary::
   :toctree:

    users
    gfks
    memo
    system
    jinja
    checkdata
    printing
    summaries
    help
    linod

Front ends
==========

.. autosummary::
   :toctree:

    bootstrap3
    extjs

Optional features
=================

.. autosummary::
   :toctree:

    about
    export_excel
    tinymce
    restful
    wkhtmltopdf
    weasyprint
    dashboard

Communication
=============

.. autosummary::
   :toctree:

    comments
    notify
    linod

Enterprise Resources
====================

.. autosummary::
   :toctree:

    importfilters
    languages
    office
    smtpd
    uploads


"""
