====================
The ``lino`` package
====================





This is the core package of the Lino framework.

This repository is an integral part of the `Lino framework
<https://www.lino-framework.org>`__, a sustainably free open-source project
maintained by the `Synodalsoft team <https://www.synodalsoft.net>`__ sponsored
by `Rumma & Ko OÜ <https://www.saffre-rumma.net>`__. Your contributions are
welcome.

- Code repository: https://gitlab.com/lino-framework/lino
- Test results: https://gitlab.com/lino-framework/lino/-/pipelines
- Feedback: https://community.lino-framework.org
- Maintainer: https://www.synodalsoft.net
- Service provider: https://www.saffre-rumma.net


